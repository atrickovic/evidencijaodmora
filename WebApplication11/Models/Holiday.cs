﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebApplication11.Models
{
    [Table("Holiday")]
    public class Holiday
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int RequestID { get; set; }

        [Display(Name ="Start Date")]
        [Required(ErrorMessage = "Please choose start date.")]
        public DateTime StartDate { get; set; }

        [Display(Name ="End Date")]
        [Required(ErrorMessage = "Please choose end date.")]
        public DateTime EndDate { get; set; }

        public int Status { get; set; }

        
        public string Employee { get; set; }
        
        public string Manager { get; set; } 
    }
}