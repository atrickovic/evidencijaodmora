﻿using System;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApplication11.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public string ManagerID { get; set; }
        public int? HolidayDays { get; set; }
        public DateTime StartDateJob { get; set; }
        public DateTime? Birthday { get; set; }
        public string Adress { get; set; }
        public string City { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Gender Gender { get; set; }
        public string Replace { get; set; }
        public int OldHoliday { get; set; }
        public int NewHoliday { get; set; }
        public DateTime OldHolidayStatus { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }

        public int CalcNewHoliday(int? holidayDays, DateTime startDateJob, int newHoliday)
        {
            DateTime date = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            float daysPerMonth = (float)holidayDays / 12;
            int days = 0;
            if (startDateJob.Year == date.Year)
            {
                switch (date.Month - startDateJob.Month + 1)
                {
                    case 1:
                        days = 0;
                        break;
                    case 2:
                        days = Convert.ToInt32(Math.Truncate(daysPerMonth));
                        break;
                    case 3:
                        days = Convert.ToInt32(Math.Truncate(daysPerMonth * 2));
                        break;
                    case 4:
                        return days = Convert.ToInt32(Math.Truncate(daysPerMonth * 3));
                        break;
                    case 5:
                        days = Convert.ToInt32(Math.Truncate(daysPerMonth * 4));
                        break;
                    case 6:
                        days = Convert.ToInt32(Math.Truncate(daysPerMonth * 5));
                        break;
                    case 7:
                        days = Convert.ToInt32(Math.Truncate(daysPerMonth * 6));
                        break;
                    case 8:
                        days = Convert.ToInt32(Math.Truncate(daysPerMonth * 7));
                        break;
                    case 9:
                        days = Convert.ToInt32(Math.Truncate(daysPerMonth * 8));
                        break;
                    case 10:
                        days = Convert.ToInt32(Math.Truncate(daysPerMonth * 9));
                        break;
                    case 11:
                        days = Convert.ToInt32(Math.Truncate(daysPerMonth * 10));
                        break;
                    case 12:
                        days = Convert.ToInt32(Math.Truncate(daysPerMonth * 11));
                        break;
                }
                if (newHoliday == 0)
                {
                    return days;
                }
                else if(newHoliday== Convert.ToInt32(Math.Truncate(daysPerMonth * (date.Month - startDateJob.Month))))
                {
                    return days;
                }
                else
                {
                    return days - (Convert.ToInt32(Math.Truncate(daysPerMonth * (date.Month - startDateJob.Month))) - newHoliday);
                }
            }
            else
            {
                switch (date.Month)
                {
                    case 1:
                        days =0;
                        break;
                    case 2:
                        days = Convert.ToInt32(Math.Truncate(daysPerMonth));
                        break;
                    case 3:
                        days = Convert.ToInt32(Math.Truncate(daysPerMonth * 2));
                        break;
                    case 4:
                        days = Convert.ToInt32(Math.Truncate(daysPerMonth * 3));
                        break;
                    case 5:
                        days = Convert.ToInt32(Math.Truncate(daysPerMonth * 4));
                        break;
                    case 6:
                        days = Convert.ToInt32(Math.Truncate(daysPerMonth * 5));
                        break;
                    case 7:
                        days = Convert.ToInt32(Math.Truncate(daysPerMonth * 6));
                        break;
                    case 8:
                        days = Convert.ToInt32(Math.Truncate(daysPerMonth * 7));
                        break;
                    case 9:
                        days = Convert.ToInt32(Math.Truncate(daysPerMonth * 8));
                        break;
                    case 10:
                        days = Convert.ToInt32(Math.Truncate(daysPerMonth * 9));
                        break;
                    case 11:
                        days = Convert.ToInt32(Math.Truncate(daysPerMonth * 10));
                        break;
                    case 12:
                        days = Convert.ToInt32(Math.Truncate(daysPerMonth * 11));
                        break;
                }
                if (newHoliday == 0)
                {
                    return days;
                }
                else if (newHoliday == Convert.ToInt32(Math.Truncate(daysPerMonth * (date.Month - 1))))
                {
                    return days;
                }
                else
                {
                    return days - (Convert.ToInt32(Math.Truncate(daysPerMonth * (date.Month - 1))) - newHoliday);
                }
            }
        }

        public int CalcOldHoliday(DateTime startDateJob,int? holidayDays, int oldHoliday, int newHoiday, DateTime oldHolidayStatus)
        {
            float daysPerMonth = (float)holidayDays / 12;
            DateTime date = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            if (startDateJob.Year == date.Year)
            {
                return 0;
            }
            else if (date.Month >= 7)
            {
                return 0;
            }
            else if (oldHolidayStatus.Year < date.Year)
            {
                return newHoiday+ Convert.ToInt32(holidayDays) - Convert.ToInt32(Math.Truncate(daysPerMonth * 11));
            }
            else
            {
                return oldHoliday;
            }
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<NonWorkingDay> nonWorkingDays { get; set; }
        public DbSet<Holiday> holidays { get; set; }


        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}