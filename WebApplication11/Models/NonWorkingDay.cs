﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebApplication11.Models
{

    [Table("NonWorkingDay")]
    public class NonWorkingDay
    {
        [Key]
        [Column("DateID")]
        [Required(ErrorMessage = "Please choose enter non working day.")]
        public DateTime DateID { get; set; }

        [Column("Description")]
        [Required(ErrorMessage = "Please enter description.")]
        public string Description { get; set; }
    }
}