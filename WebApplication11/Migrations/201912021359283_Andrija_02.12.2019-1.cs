namespace WebApplication11.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Andrija_021220191 : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.Holiday");
            AlterColumn("dbo.Holiday", "RequestID", c => c.String(nullable: false, maxLength: 128));
            AddPrimaryKey("dbo.Holiday", "RequestID");
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.Holiday");
            AlterColumn("dbo.Holiday", "RequestID", c => c.String(nullable: false, maxLength: 128));
            AddPrimaryKey("dbo.Holiday", "RequestID");
        }
    }
}
