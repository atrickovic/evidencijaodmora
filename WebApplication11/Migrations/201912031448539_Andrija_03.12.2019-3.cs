namespace WebApplication11.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Andrija_031220193 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Holiday", "Manager", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Holiday", "Manager");
        }
    }
}
