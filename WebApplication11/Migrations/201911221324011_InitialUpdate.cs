namespace WebApplication11.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialUpdate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "Manager", c => c.Int(nullable: false));
            AddColumn("dbo.AspNetUsers", "HolidayDays", c => c.Int(nullable: false));
            AddColumn("dbo.AspNetUsers", "StartDateJob", c => c.DateTime(nullable: false));
            AddColumn("dbo.AspNetUsers", "Birthday", c => c.DateTime(nullable: false));
            AddColumn("dbo.AspNetUsers", "Adress", c => c.String());
            AddColumn("dbo.AspNetUsers", "City", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "City");
            DropColumn("dbo.AspNetUsers", "Adress");
            DropColumn("dbo.AspNetUsers", "Birthday");
            DropColumn("dbo.AspNetUsers", "StartDateJob");
            DropColumn("dbo.AspNetUsers", "HolidayDays");
            DropColumn("dbo.AspNetUsers", "Manager");
        }
    }
}
