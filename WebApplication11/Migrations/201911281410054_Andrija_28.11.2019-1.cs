namespace WebApplication11.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Andrija_281120191 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "OldHoliday", c => c.Int());
            AddColumn("dbo.AspNetUsers", "NewHoliday", c => c.Int());
            AddColumn("dbo.AspNetUsers", "OldHolidayStatus", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "OldHolidayStatus");
            DropColumn("dbo.AspNetUsers", "NewHoliday");
            DropColumn("dbo.AspNetUsers", "OldHoliday");
        }
    }
}
