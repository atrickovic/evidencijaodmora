namespace WebApplication11.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Andrija_291120192 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "ManagerID", c => c.String());
            AddColumn("dbo.AspNetUsers", "Replace", c => c.String());
            DropColumn("dbo.AspNetUsers", "Manager");
            DropColumn("dbo.AspNetUsers", "ReplaceID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AspNetUsers", "ReplaceID", c => c.Int());
            AddColumn("dbo.AspNetUsers", "Manager", c => c.Int());
            DropColumn("dbo.AspNetUsers", "Replace");
            DropColumn("dbo.AspNetUsers", "ManagerID");
        }
    }
}
