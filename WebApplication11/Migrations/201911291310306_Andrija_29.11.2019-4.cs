namespace WebApplication11.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Andrija_291120194 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.NonWorkingDay",
                c => new
                    {
                        DateID = c.DateTime(nullable: false),
                        Description = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.DateID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.NonWorkingDay");
        }
    }
}
