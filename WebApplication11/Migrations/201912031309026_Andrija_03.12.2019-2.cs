namespace WebApplication11.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Andrija_031220192 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.AspNetUsers", "OldHoliday", c => c.Int(nullable: false));
            AlterColumn("dbo.AspNetUsers", "NewHoliday", c => c.Int(nullable: false));
            AlterColumn("dbo.AspNetUsers", "OldHolidayStatus", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.AspNetUsers", "OldHolidayStatus", c => c.DateTime());
            AlterColumn("dbo.AspNetUsers", "NewHoliday", c => c.Int());
            AlterColumn("dbo.AspNetUsers", "OldHoliday", c => c.Int());
        }
    }
}
