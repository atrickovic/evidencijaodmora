﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication11.ViewModels
{
    public class HolidayViewModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }        
        public string Email { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int RequestID { get; set; }
        public bool RequestApproved { get; set; } = false;
    }
}