﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication11.Models;

namespace WebApplication11.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationDbContext context = new ApplicationDbContext();

        [HttpGet]
        public ActionResult Index()
        {
            ApplicationUser currentUser = new ApplicationUser();
            string logUserEmail = User.Identity.GetUserName();

            if (logUserEmail != "")
            {
                currentUser = context.Users.Where(x => x.Email.Equals(logUserEmail)).FirstOrDefault();
                string logUserName1 = currentUser.FirstName;
                string logUserName2 = currentUser.LastName;
                ViewBag.Name = logUserName1 + " " + logUserName2;

                
                if (!currentUser.HolidayDays.Equals(null))
                {

                    if (currentUser.OldHolidayStatus.Year<DateTime.Now.Year)
                    {
                        currentUser.OldHoliday = currentUser.CalcOldHoliday(currentUser.StartDateJob,currentUser.HolidayDays, currentUser.OldHoliday, currentUser.NewHoliday, currentUser.OldHolidayStatus);
                        currentUser.OldHolidayStatus = DateTime.Now;
                        context.Entry(currentUser).State = EntityState.Modified;
                        ViewBag.OldHoliday = currentUser.CalcOldHoliday(currentUser.StartDateJob,currentUser.HolidayDays, currentUser.OldHoliday, currentUser.NewHoliday, currentUser.OldHolidayStatus);
                        context.SaveChanges();
                    }
                    else if(DateTime.Now.Month>7)
                    {
                        currentUser.OldHoliday = currentUser.CalcOldHoliday(currentUser.StartDateJob,currentUser.HolidayDays, currentUser.OldHoliday, currentUser.NewHoliday, currentUser.OldHolidayStatus);
                        context.Entry(currentUser).State = EntityState.Modified;
                        ViewBag.OldHoliday = currentUser.CalcOldHoliday(currentUser.StartDateJob,currentUser.HolidayDays, currentUser.OldHoliday, currentUser.NewHoliday, currentUser.OldHolidayStatus);
                        context.SaveChanges();
                    }
                    else
                    {
                        ViewBag.OldHoliday = currentUser.OldHoliday;
                    }
                    if (currentUser.NewHoliday != currentUser.CalcNewHoliday(currentUser.HolidayDays, currentUser.StartDateJob, currentUser.NewHoliday))
                    {
                        currentUser.NewHoliday = currentUser.CalcNewHoliday(currentUser.HolidayDays, currentUser.StartDateJob, currentUser.NewHoliday);
                        context.Entry(currentUser).State = EntityState.Modified;
                        context.SaveChanges();
                        ViewBag.NewHoliday= currentUser.CalcNewHoliday(currentUser.HolidayDays, currentUser.StartDateJob, currentUser.NewHoliday);
                    }
                    else
                    {
                        ViewBag.NewHoliday = currentUser.NewHoliday;
                    }
                    ViewBag.Holiday = ViewBag.OldHoliday + ViewBag.NewHoliday;
                }
                else
                {
                    ViewBag.Holiday = 0;
                    ViewBag.OldHoliday = 0;
                    ViewBag.NewHoliday = 0;
                }
   
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
            return View();
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Index(FormCollection collection, Holiday m)
        {
            ApplicationUser currentUser = new ApplicationUser();
            Holiday hol = new Holiday();
            string logUserEmail = User.Identity.GetUserName();
            try
            {
                //context.holidays.Add(new Holiday {
                //    StartDate = m.StartDate,
                //    EndDate = m.EndDate,
                //    Employee = currentUser.Id,
                //    Manager = currentUser.ManagerID
                //});

                if (logUserEmail != "")
                {
                    currentUser = context.Users.Where(x => x.Email.Equals(logUserEmail)).FirstOrDefault();
                    string logUserName1 = currentUser.FirstName;
                    string logUserName2 = currentUser.LastName;
                    ViewBag.Name = logUserName1 + " " + logUserName2;

                    ViewBag.Holiday = currentUser.OldHoliday + currentUser.NewHoliday;
                    ViewBag.OldHoliday = currentUser.OldHoliday;
                    ViewBag.NewHoliday = currentUser.NewHoliday;

                    context.holidays.Add(new Holiday
                    {
                        StartDate = m.StartDate,
                        EndDate = m.EndDate,
                        Employee = currentUser.Id,
                        Manager = currentUser.ManagerID
                    });
                }
                else
                {
                    return RedirectToAction("Login", "Account"); ;
                }
                context.SaveChanges();
                return View();
            }
            catch(Exception ex)
            {
                return View();
            }
            
        }


        [HttpGet]
        public ActionResult About()
        {
            

            ApplicationUser currentUser = new ApplicationUser();
            string logUserEmail = User.Identity.GetUserName();

            if (logUserEmail != "")
            {
                currentUser = context.Users.Where(x => x.Email.Equals(logUserEmail)).FirstOrDefault();
                string logUserName1 = currentUser.FirstName;
                string logUserName2 = currentUser.LastName;
                ViewBag.Message = logUserName1 + " " + logUserName2;
                ViewBag.FirstName = currentUser.FirstName;
                ViewBag.LastName = currentUser.LastName;
                ViewBag.Gender = currentUser.Gender;
                if(currentUser.Adress!=null)
                {
                    ViewBag.Adress = currentUser.Adress;
                }
                else
                {
                    ViewBag.Adress = "";
                }
                if (currentUser.Birthday != null)
                {
                    ViewBag.Birthday =Convert.ToString(currentUser.Birthday);
                }
                else
                {
                    ViewBag.Birthday = "";
                }

                if (currentUser.City != null)
                {
                    ViewBag.City = currentUser.City;
                }
                else
                {
                    ViewBag.City = "";
                }

            }
            else
            {
                return RedirectToAction("Login", "Account");
            }

            return View();
        }

        [HttpPost]
        //[AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult About(AdditionalInformation m)
        {
            ApplicationUser user = new ApplicationUser();
            string logUserEmail = User.Identity.GetUserName();
            try
            {
                //var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
                //var user = UserManager.FindByName(logUserEmail);
                user = context.Users.Where(x => x.Email.Equals(logUserEmail)).FirstOrDefault();


                if (user.FirstName != m.FirstName)
                {
                    user.FirstName = m.FirstName;
                }
                if (user.LastName != m.LastName)
                {
                    user.LastName = m.LastName;
                }

                if (user.Adress != m.Adress)
                {
                    user.Adress = m.Adress;
                }

                if (user.City != m.City)
                {
                    user.City = m.City;
                }

                if (user.Gender != m.Gender)
                {
                    user.Gender = m.Gender;
                }

                if (user.Birthday != m.Birthday && !user.Birthday.Equals(null))
                {
                    user.Birthday = m.Birthday;
                }

                context.Entry(user).State = EntityState.Modified;
                context.SaveChanges();

                ViewBag.ResultMessage = "Data is updated!";

                if (logUserEmail != "")
                {
                    //currentUser = context.Users.Where(x => x.Email.Equals(logUserEmail)).FirstOrDefault();
                    //string logUserName1 = currentUser.FirstName;
                    //string logUserName2 = currentUser.LastName;
                    ViewBag.Message = user.FirstName + " " + user.LastName;
                    ViewBag.FirstName = user.FirstName;
                    ViewBag.LastName = user.LastName;
                    ViewBag.Gender = user.Gender;
                    if (user.Adress != null)
                    {
                        ViewBag.Adress = user.Adress;
                    }
                    else
                    {
                        ViewBag.Adress = "";
                    }
                    if (user.Birthday != null)
                    {
                        ViewBag.Birthday = Convert.ToString(user.Birthday);
                    }
                    else
                    {
                        ViewBag.Birthday = "";
                    }

                    if (user.City != null)
                    {
                        ViewBag.City = user.City;
                    }
                    else
                    {
                        ViewBag.City = "";
                    }

                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }

                return View();
            }
            //ViewBag.Message = "Your application description page.";
            catch
            {
                return View();
            }
        }

        [HttpGet]
        public ActionResult Contact()
        {
            ApplicationUser currentUser = new ApplicationUser();
            string logUserEmail = User.Identity.GetUserName();

            if (logUserEmail != "")
            {
                currentUser = context.Users.Where(x => x.Email.Equals(logUserEmail)).FirstOrDefault();
                //string logUserName1 = currentUser.FirstName;
                //string logUserName2 = currentUser.LastName;
                //ViewBag.Message = logUserName1 + " " + logUserName2;
                ViewBag.FirstName = currentUser.FirstName;
                ViewBag.LastName = currentUser.LastName;

                if (currentUser.HolidayDays != null)
                {
                    ViewBag.HolidayDays = currentUser.HolidayDays;
                }

                if (currentUser.ManagerID!=null)
                {
                    ViewBag.Manager = currentUser.ManagerID;
                }

                if (currentUser.Replace!=null)
                {
                    ViewBag.ReplaceID = currentUser.Replace;
                }

            }
            else
            {
                return RedirectToAction("Login", "Account");
            }

            return View();
        }

        [HttpPost]
        //[AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Contact(AdditionalInformation m)
        {
            ApplicationUser user = new ApplicationUser();
            string logUserEmail = User.Identity.GetUserName();
            try
            {
                //var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
                //var user = UserManager.FindByName(logUserEmail);
                user = context.Users.Where(x => x.Email.Equals(logUserEmail)).FirstOrDefault();


                if (user.ManagerID != m.Manager)
                {
                    user.ManagerID = m.Manager;
                }
                if (user.Replace!=m.Replace)
                {
                    user.Replace = m.Replace;
                }

                if (user.HolidayDays != m.HolidayDays)
                {
                    user.HolidayDays = m.HolidayDays;
                }

                
                

                context.Entry(user).State = EntityState.Modified;
                context.SaveChanges();

                ViewBag.ResultMessage = "Data is updated!";

                if (logUserEmail != "")
                {
                    user = context.Users.Where(x => x.Email.Equals(logUserEmail)).FirstOrDefault();
                    string logUserName1 = user.FirstName;
                    string logUserName2 = user.LastName;
                    //ViewBag.Message = logUserName1 + " " + logUserName2;
                    ViewBag.FirstName = user.FirstName;
                    ViewBag.LastName = user.LastName;

                    if (user.HolidayDays != null)
                    {
                        ViewBag.HolidayDays = user.HolidayDays;
                    }

                    if (user.ManagerID != null)
                    {
                        ViewBag.Manager = user.ManagerID;
                    }

                    if (user.Replace != null)
                    {
                        ViewBag.ReplaceID = user.Replace;
                    }

                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }

                return View();
            }
            //ViewBag.Message = "Your application description page.";
            catch
            {
                return View();
            }
        }

        public ActionResult GetEmployeeList(string q)
        {

            List<string> list = new List<string>();
            
            if (!(string.IsNullOrEmpty(q) || string.IsNullOrWhiteSpace(q)))
            {
                list = context.Users.Where(x => x.Email.ToLower().StartsWith(q.ToLower())).Select(p => p.Email).ToList(); 
            }
            return Json(new { items = list }, JsonRequestBehavior.AllowGet);
        }
    }
}