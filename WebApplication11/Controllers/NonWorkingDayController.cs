﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using WebApplication11.Models;

namespace WebApplication11.Controllers
{
    [Authorize(Roles = "admin")]
    public class NonWorkingDayController : Controller
    {
        ApplicationDbContext NonWorkingDayContext = new ApplicationDbContext();

        // GET: Upload
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        
        public ActionResult UploadFile()
        {
            return View();
        }
        [HttpPost]
        public ActionResult UploadFile(HttpPostedFileBase file)
        {
            StreamReader datNonWork = null;
            try
            {

                if (file.ContentLength > 0)
                {
                    string _FileName = Path.GetFileName(file.FileName);
                    string _path = Path.Combine(Server.MapPath("~/UploadedFiles"), _FileName);
                    file.SaveAs(_path);

                    datNonWork = new StreamReader(_path);
                    string rowString;
                    while ((rowString = datNonWork.ReadLine()) != null)
                    {
                        NonWorkingDay nonWorkingDay = new NonWorkingDay();
                        string[] str = rowString.Split(';');
                        nonWorkingDay.DateID = Convert.ToDateTime(str[0]);
                        nonWorkingDay.Description = str[1];
                        NonWorkingDayContext.nonWorkingDays.Add(nonWorkingDay);

                    }
                    NonWorkingDayContext.SaveChanges();
                }

                ViewBag.Message = "File Uploaded Successfully!!";
                return View();
            }
            catch (Exception ex)
            {

                ViewBag.Message = "File upload failed!! " + ex.Message;
                return View();
            }
            finally
            {
                datNonWork.Close();
            }
        }





    }
}
