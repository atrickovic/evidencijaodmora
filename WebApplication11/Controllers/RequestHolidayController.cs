﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication11.Models;
using WebApplication11.ViewModels;

namespace WebApplication11.Controllers
{
    
    public class RequestHolidayController : Controller
    {
        ApplicationDbContext context = new ApplicationDbContext();
        
        // GET: RequestHoliday
        [HttpGet]
        [Authorize(Roles ="manager, admin")]
        public ActionResult Index()
        {
            DataSet dt = new DataSet();
            //List<ApplicationUser> users = new List<ApplicationUser>();
            ApplicationUser currentUser = new ApplicationUser();
            string logUserEmail = User.Identity.GetUserName();
            if (logUserEmail != "")
            {
                currentUser = context.Users.Where(x => x.Email.Equals(logUserEmail)).FirstOrDefault();

                //var query = from usr in context.Users
                //            join  hol in context.holidays on usr equals hol.Employee
                //            where (hol.Status == 0 && hol.Manager.Equals(currentUser.Id))
                //            select new (fName=usr.FirstName, usr.LastName, usr.Email, hol.StartDate, hol.EndDate);

                var results = (from h in context.holidays.Where(p=>p.Manager==currentUser.Id && p.Status==0)
                              join u in context.Users
                              on h.Employee equals u.Id                           
                              select new HolidayViewModel
                              {
                                  FirstName = u.FirstName,
                                  LastName = u.LastName,
                                  Email = u.Email,
                                  StartDate = h.StartDate,
                                  EndDate = h.EndDate,
                                  RequestID = h.RequestID
                              }).ToList();

                //var results = context.Users.Join(context.holidays, usr => usr.Id, hol => hol.Employee,
                //    (usr, hol) => new { usr, hol })
                //    .Where(m => m.hol.Status == 0 && m.hol.Manager == logUserEmail).
                //    Select(r => new
                //    {
                //        fName = r.usr.FirstName,
                //        lName = r.usr.LastName,
                //        eMail = r.usr.Email,
                //        sDate = r.hol.StartDate,
                //        eDate = r.hol.EndDate
                //    }).ToList();
                return View(results);
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }

        }

        //[HttpPost]
        //public ActionResult Index(FormCollection collection)
        //{
        //    return View();
        //}
    }
}