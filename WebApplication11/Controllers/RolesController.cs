﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication11.Models;

namespace WebApplication11.Controllers
{
    [Authorize(Roles = "admin")]
    public class RolesController : Controller
    {
        private ApplicationDbContext context = new ApplicationDbContext();
        
        // GET: Roles
        public ActionResult Index()
        {
            return View(context.Roles.ToList());
        }

        
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                context.Roles.Add(new IdentityRole()
                {
                    Name = collection["RoleName"]
                });
                context.SaveChanges();
                ViewBag.ResultMessage = "Role created successfully !";
                return View("Create");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Delete(string RoleName)
        {
            var thisRole = context.Roles.Where(r => r.Name.Equals(RoleName, StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();
            context.Roles.Remove(thisRole);
            context.SaveChanges();
            return RedirectToAction("Create");
        }

        // GET: /Roles/Edit
        public ActionResult Edit(string roleName)
        {
            var thisRole = context.Roles.Where(r => r.Name.Equals(roleName, StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();

            return View(thisRole);
        }

        
        // POST: /Roles/Edit
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Microsoft.AspNet.Identity.EntityFramework.IdentityRole role)
        {
            try
            {
                context.Entry(role).State = System.Data.Entity.EntityState.Modified;
                context.SaveChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: /Roles/Manage
        public ActionResult Manage()
        {
            var items = context.Roles.ToList();
            if (items != null)
            {
                ViewBag.data = items;
            }
            

            return View();
            //return View(context.Roles.ToList());
        }


        // POST: /Roles/Manage
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Manage(FormCollection collection)//, IdentityUserRole i)
        {

            //try
            //{
            //    context.Entry(role).State = System.Data.Entity.EntityState.Modified;
            //    context.SaveChanges();

            //    return RedirectToAction("Index");
            //}
            //catch
            //{
            //    return View();
            //}

            var items = context.Roles.ToList();
            if (items != null)
            {
                ViewBag.data = items;
            }

            try
            {
                var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));

                var user = UserManager.FindByName(collection["Employee"]);
                UserManager.AddToRole(user.Id, collection["RoleListBox"]);
                context.SaveChanges();

                ViewBag.ResultMessage = "Role added successfully !";
                return View("Manage");
            }
            catch
            {
                return View("Manage");
            }
            
        }
    }
}